# Insomnia workspace for Jira Server REST API

## Use 

To import the Insomnia Workspace:

**Note: SDK workspace file references an atlas-run-standalone instance running on port 2990**

Import the file from the Jira version you wish to work with; 7.6.1, 8.0.0, 8.1.0, etc.

8.1.0/Jira-Server-8.1.json is the current version.

![](import.png)

The Postman description can be imported to a new or existing workspace:

**Import Workspace**: Jira-Server-8.1.json

**Import Postman export into an existing Workspace**: Jira-Server-8.1.json


### New Workspace
![](new-workspace.png)

### Import
![](import-type.png)

Upon completion, you should see:

![](result.png)

Access the APIs from the panel on the left. Authentication is set per-request in the Auth tab,

![](apis.png)


## Process

Current and recent versions were created with APIMATIC.

A similar process for Postman:

- Download the wadl @ [https://docs.atlassian.com/software/jira/docs/api/REST/8.1.0/jira-rest-plugin.wadl](https://docs.atlassian.com/software/jira/docs/api/REST/8.1.0/jira-rest-plugin.wadl)

- Import to Postman (You can stick with it, if that works for you.)

- Export as Postman v2 format: Jira Server 8.1.0.postman_collection.json

- imported to Insomnia as "Jira Server 8.1.0" workspace: Jira-Server-8.1.0_2018-07-03.json

# Install Insomnia on Debian/Ubuntu

	# Add to sources
	echo "deb https://dl.bintray.com/getinsomnia/Insomnia /" \
	    | sudo tee -a /etc/apt/sources.list.d/insomnia.list

	# Add public key used to verify code signature
	wget --quiet -O - https://insomnia.rest/keys/debian-public.key.asc \
	    | sudo apt-key add -

	# Refresh repository sources and install Insomnia
	sudo apt-get update
	sudo apt-get install insomnia



## More
**See:** [I have Insomnia (and I like it)](https://www.linkedin.com/pulse/i-have-insomnia-like-william-kennedy/) @ [https://www.linkedin.com/pulse/i-have-insomnia-like-william-kennedy/](https://www.linkedin.com/pulse/i-have-insomnia-like-william-kennedy/)

**Working with Jira APIs**: [https://developer.atlassian.com/server/jira/platform/rest-apis/](https://developer.atlassian.com/server/jira/platform/rest-apis/)

 
